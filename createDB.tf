// Configure the Google Cloud provider
provider "google" {
  credentials = "${file("credentialsAdmin.json")}"
  project     =  "${var.project}"
  region      = "${var.region}"
}



//Create a simple database tier db-f1-micro using MySQL
resource "google_sql_database_instance" "master" {
  name = "${var.instance_name_sql}" 
  database_version = "${var.database_version}"
  region = "${var.region_db}"
  
  settings {
    tier = "${var.tier_db}"
  }
}




