provider "kubernetes" {}

resource "kubernetes_replication_controller" "nodeJS" {
  metadata {
    name = "node-js-hello"
    labels {
      App = "nodeJSHello"
    }
  }

  spec {
    replicas = 2
    selector {
      App = "nodeJSHello"
    }
    template {
      container {
        image = "heroku/nodejs-hello-world"
        name  = "node-js-heroku-hello"

        port {
          container_port = 3000
        }

       
       //Removed to deploy on cluster with limited resources  	
       // resources {
       //   limits {
       //     cpu    = "0.5"
       //     memory = "512Mi"
       //   }
       //   requests {
       //     cpu    = "250m"
       //     memory = "50Mi"
       //   }
       // }

      }
    }
  }
}

resource "kubernetes_service" "nodeJS" {
  metadata {
    name = "node-js-service"
  }
  spec {
    selector {
      App = "nodeJSHello"
    }
    port {
      port = 80
      target_port = 3000
    }

    type = "LoadBalancer"
  }
}
